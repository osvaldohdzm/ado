﻿using System.Collections.Generic;
using AdopticomApp.Models;
using Android.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace AdopticomApp.Droid.Views
{
    public class AnimalsListAdapter : BaseAdapter
    {
        private readonly List<AnimalPublications> Animals;
        protected Activity ctx;

        public AnimalsListAdapter(Activity _ctx, List<AnimalPublications> _Animals)
        {
            ctx = _ctx;
            Animals = _Animals;
        }

        public override int Count => Animals.Count;

        public override Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;


            if (view == null)
                view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.Animals_list_row, parent, false);
            var Animal = Animals[position];

            view.FindViewById<ImageView>(Resource.Id.imgView).SetImageResource(Animal.AnimalPrincipalPicture );
            view.FindViewById<TextView>(Resource.Id.textHeader).Text = Animal.Name;
            view.FindViewById<TextView>(Resource.Id.textSub).Text = Animal.Breeds;

            return view;
        }
    }
}