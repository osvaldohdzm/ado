using System.Collections.Generic;
using AdopticomApp.Models;
using Android.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace AdopticomApp.Droid.Views
{
    internal class ListViewAdapter : BaseAdapter
    {
        private readonly List<User> lstAccounts;
        protected Activity ctx;


        public ListViewAdapter(Activity _ctx, List<User> lstAccounts)
        {
            ctx = _ctx;
            this.lstAccounts = lstAccounts;
        }

        public override int Count => lstAccounts.Count;

        public override Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;

            if (view == null)
                view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.list_item, parent, false);

            var txtUser = view.FindViewById<TextView>(Resource.Id.list_name);
            var txtEmail = view.FindViewById<TextView>(Resource.Id.list_email);

            if (lstAccounts.Count > 0)
            {
                txtUser.Text = lstAccounts[position].Name;
                txtEmail.Text = lstAccounts[position].Email;
            }

            return view;
        }
    }
}