using System;
using AdopticomApp.Models;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace AdopticomApp.Droid.Views
{
    public class AnimalViewHolder : RecyclerView.ViewHolder
    {
        public AnimalViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            AnimalPrincipalPicture  = itemView.FindViewById<ImageView>(Resource.Id.imageView);
            Name = itemView.FindViewById<TextView>(Resource.Id.textView);
            Breeds = itemView.FindViewById<TextView>(Resource.Id.textView2);
            itemView.Click += (sender, e) => listener(LayoutPosition);
        }

        public ImageView AnimalPrincipalPicture  { get; }

        public TextView Name { get; }

        public TextView Breeds { get; }
    }

    public class AdapterEventArgs : EventArgs
    {
        public int Item { get; set; }

        public int NumberR { get; set; }
    }

    public class AnimalCatalogAdapter : RecyclerView.Adapter
    {
        public AnimalCatalog mAnimalCatalog;
        public ViewGroup mparent;
        public int numberRecyclerview;

        public AnimalCatalogAdapter(AnimalCatalog AnimalCatalog, int nRecyclerview)
        {
            mAnimalCatalog = AnimalCatalog;
            numberRecyclerview = nRecyclerview;
        }

        public override int ItemCount => mAnimalCatalog.NumAnimals;

        public event EventHandler<int> ItemClick;

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            mparent = parent;
            var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.photo_card_view, parent, false);
            var vh = new AnimalViewHolder(itemView, OnClick);
            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var vh = holder as AnimalViewHolder;
            vh.AnimalPrincipalPicture .SetImageResource(mAnimalCatalog[position].AnimalPrincipalPicture );
            vh.Name.Text = mAnimalCatalog[position].Name;
            vh.Breeds.Text = mAnimalCatalog[position].Breeds;
        }

        private void OnClick(int position)
        {
            ItemClick?.Invoke(this, position);
        }
    }
}