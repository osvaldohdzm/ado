﻿using Android.OS;
using Android.Support.V4.App;
using Android.Views;

namespace AdopticomApp.Droid.Views
{
    public class Profilefragment : Fragment
    {
        public static Profilefragment NewInstance()
        {
            var frag1 = new Profilefragment
            {
                Arguments = new Bundle()
            };
            return frag1;
        }


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.profile_association, container, false);


            return view;
        }
    }
}