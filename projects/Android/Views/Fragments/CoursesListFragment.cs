﻿using System.Collections.Generic;
using AdopticomApp.Data;
using AdopticomApp.Models;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;

namespace AdopticomApp.Droid.Views
{
    public class AnimalsListFragment : Fragment
    {
        private AnimalsListAdapter adapter;
        private ListView AnimalListView;
        private List<AnimalPublications> Animals;
        private Context myContext;
        private View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public static AnimalsListFragment NewInstance()
        {
            var frag3 = new AnimalsListFragment {Arguments = new Bundle()};
            return frag3;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.listscreen, container, false);

            myContext = view.Context;
            var activity = (Activity) view.Context;

            AnimalListView = view.FindViewById<ListView>(Resource.Id.myListView);

            Animals = GetAnimals();
            adapter = new AnimalsListAdapter(Activity, Animals);

            AnimalListView.Adapter = adapter;

            AnimalListView.ItemClick += AnimalListView_ItemClick;


            return view;
        }

        private void AnimalListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var selected = Animals[e.Position];
            Toast.MakeText(myContext, selected.ToString(), ToastLength.Short).Show();
        }

        private List<AnimalPublications> GetAnimals()
        {
            SQLiteDbHandler sQLite;
            List<AnimalPublications> animalPublications;
            sQLite = new SQLiteDbHandler();
            animalPublications = sQLite.selectAnimalPublications();


            return animalPublications;
        }
    }
}