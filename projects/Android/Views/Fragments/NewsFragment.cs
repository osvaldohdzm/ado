﻿using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Webkit;

namespace AdopticomApp.Droid.Views
{
    public class NewsFragment : Fragment
    {
        private WebView web_view;

        public static NewsFragment NewInstance()
        {
            var frag1 = new NewsFragment
            {
                Arguments = new Bundle()
            };
            return frag1;
        }


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.news_fragment, container, false);

            web_view = view.FindViewById<WebView>(Resource.Id.webview);
            web_view.Settings.JavaScriptEnabled = true;
            web_view.SetWebViewClient(new HelloWebViewClient());
            web_view.LoadUrl("https://twitter.com/mascotaadopcion?lang=es");


            return view;
        }
    }

    public class HelloWebViewClient : WebViewClient
    {
        public override bool ShouldOverrideUrlLoading(WebView view, string url)
        {
            view.LoadUrl(url);
            return false;
        }
    }
}