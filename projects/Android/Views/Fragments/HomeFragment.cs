﻿using System;
using System.Collections.Generic;
using AdopticomApp.Data;
using AdopticomApp.Droid.Libraries;
using AdopticomApp.Models;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace AdopticomApp.Droid.Views
{
    public class HomeFragment : Fragment
    {
        SQLiteDbHandler sQLite;
        List<AnimalPublications> animalPublications;
        List<AnimalPublications> dogsCatalog;
        List<AnimalPublications> catsCatalog;

        // ViewPager
        private readonly Random RANDOM = new Random();
        public ImageAdapter adapter;

        // Adapter that accesses the data set
        private AnimalCatalogAdapter mAdapter;
        private AnimalCatalogAdapter mAdapter2;


        // Content data that is managed by the adapter
        private AnimalCatalog mAnimalCatalog;
        private AnimalCatalog mAnimalCatalog2;
        public PageIndicator mIndicator;

        // Layout manager that lays out each card in the RecyclerView:
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView.LayoutManager mLayoutManager2;
        public ViewPager mPager;

        private RecyclerView mRecyclerView;
        private RecyclerView mRecyclerView2;

        private View view;

        public static HomeFragment NewInstance()
        {
            var frag1 = new HomeFragment
            {
                Arguments = new Bundle()
            };
            return frag1;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            sQLite = new SQLiteDbHandler();
            animalPublications = sQLite.selectAnimalPublications();

            dogsCatalog = new List<AnimalPublications>();
            catsCatalog = new List<AnimalPublications>();

            for (int i = 0; i < animalPublications.Count; i++)
            {
                if (animalPublications[i].Animal == "Dog")
                {
                    dogsCatalog.Add(animalPublications[i]);
                }
                else if (animalPublications[i].Animal == "Cat")
                {
                    catsCatalog.Add(animalPublications[i]);
                }
            }
    }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            view = inflater.Inflate(Resource.Layout.home_fragment, container, false);

            var text1 = view.FindViewById<TextView>(Resource.Id.textView1);
            text1.Click += delegate { SectionNameClick(1); };
            var text2 = view.FindViewById<TextView>(Resource.Id.textView2);
            text2.Click += delegate { SectionNameClick(2); };

            // Instantiate the photo catalog:
            mAnimalCatalog = new AnimalCatalog(dogsCatalog);
            mAnimalCatalog2 = new AnimalCatalog(catsCatalog);


            // Get our RecyclerView layout:
            mRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            mRecyclerView2 = view.FindViewById<RecyclerView>(Resource.Id.recyclerView2);


            // -------------------------------------------------
            // Layout Manager Setup
            // Use the built-in linear layout manager:
            // Original code
            // 
            // mLayoutManager = new LinearLayoutManager(view.Context);
            //
            // Modidified code for orientation of RecyclerView
            mLayoutManager = new LinearLayoutManager(view.Context, LinearLayoutManager.Horizontal, false);
            mLayoutManager2 = new LinearLayoutManager(view.Context, LinearLayoutManager.Horizontal, false);

            // Or use the built-in grid layout manager (two horizontal rows):
            // mLayoutManager = new GridLayoutManager
            //        (this, 2, GridLayoutManager.Horizontal, false);

            // Plug the layout manager into the RecyclerView:           
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mRecyclerView2.SetLayoutManager(mLayoutManager2);

            // -------------------------------------------------
            // Adapter Setup:

            // Create an adapter for the RecyclerView, and pass it the
            // data set (the photo catalog) to manage:
            mAdapter = new AnimalCatalogAdapter(mAnimalCatalog, 1);
            mAdapter2 = new AnimalCatalogAdapter(mAnimalCatalog2, 2);

            // Register the item click handler (below) with the adapter:
            mAdapter.ItemClick += OnItemClick;
            mAdapter2.ItemClick += OnItemClick;


            // Plug the adapter into the RecyclerView:
            mRecyclerView.SetAdapter(mAdapter);
            mRecyclerView2.SetAdapter(mAdapter2);

           


            return view;
        }

        // Handler for the item click event:
        private void OnItemClick(object sender, int position)
        {
            var theAdapter = (AnimalCatalogAdapter) sender;


            // Display a toast that briefly shows the enumeration of the selected photo:

            var photoNum = position + 1;
            
            Toast.MakeText(View.Context, "This is photo number" + photoNum + " es " + theAdapter.numberRecyclerview,
                ToastLength.Short).Show();

            
            if (theAdapter.numberRecyclerview == 1)
            {
                var parentActivity = (HomeActivity)Activity;
                var editPublicationActivity = new Intent(view.Context, typeof(EditPublicationActivity));
                editPublicationActivity.PutExtra("selectePublicationId", dogsCatalog[position].Id);
                parentActivity.StartActivity(editPublicationActivity);
            }
            else if (theAdapter.numberRecyclerview == 2)
            {
                var parentActivity = (HomeActivity) Activity;
                var editPublicationActivity = new Intent(view.Context, typeof(EditPublicationActivity));
                editPublicationActivity.PutExtra("selectePublicationId", catsCatalog[position].Id);
                parentActivity.StartActivity(editPublicationActivity);
            }
        }

        public void SectionNameClick(int positionNumber)
        {
            var myActivity = (HomeActivity) Activity;
            myActivity.ShowListFragment();
        }
    }
}