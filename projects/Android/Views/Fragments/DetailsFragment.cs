﻿using Android.Content;
using Android.Net;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using AdopticomApp.Data;
using AdopticomApp.Droid.Libraries;
using AdopticomApp.Models;
using System;
using Android.Support.V4.View;
using System.Collections.Generic;

namespace AdopticomApp.Droid.Views
{
    public class DetailsFragment : Fragment
    {
        // ViewPager
        private readonly Random RANDOM = new Random();
        public ImageAdapter adapter;
        public ViewPager mPager;
        public PageIndicator mIndicator;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.details_fragment, container, false);

            var butt = view.FindViewById<Button>(Resource.Id.button1);

            var txtv1 = view.FindViewById<TextView>(Resource.Id.editText1);
            var txtv2 = view.FindViewById<TextView>(Resource.Id.editText2);
            var txtv3 = view.FindViewById<TextView>(Resource.Id.editText3);
            var txtv4 = view.FindViewById<TextView>(Resource.Id.editText4);

            int mParam2 = 0;
            List<AnimalPublications> animalPub = new List<AnimalPublications>();
            if (Arguments != null)
            {
                mParam2 = Arguments.GetInt("publicationId");

                SQLiteDbHandler sQLite;
                sQLite = new SQLiteDbHandler();
                animalPub = sQLite.selectAnimalPublication(mParam2);

                txtv1.Text = animalPub[0].Name;
                txtv2.Text = animalPub[0].Breeds;
                txtv3.Text = animalPub[0].Health;
                txtv4.Text = animalPub[0].Description;
            }          


            butt.Click += delegate
            {
                // telephone
                /*
                var uri = Android.Net.Uri.Parse("tel:15625043");
                var intent = new Intent(Intent.ActionDial, uri);
                StrtActivity(intent);
                */

                var email = new Intent(Android.Content.Intent.ActionSend);
                email.PutExtra(Android.Content.Intent.ExtraEmail, new string[] {
            "osvaldo.hdz.m@outlook.com",
        });
                email.PutExtra(Android.Content.Intent.ExtraCc, new string[] {
            "osvaldo.hdz.m@outlook.com",
        });
                email.PutExtra(Android.Content.Intent.ExtraSubject, "Adopticom: Mensaje");
                email.PutExtra(Android.Content.Intent.ExtraText, "Hola bienvenido a adopticom!");
                email.SetType("message/rfc822");
                StartActivity(email);
            };


            // Viewpager -----------------
            // mAdapter = new TestFragmentAdapter (SupportFragmentManager);
           int[] imageList =
        {
            Resource.Drawable.perro01,
            Resource.Drawable.perro01,
            Resource.Drawable.perro01
        };
        adapter = new ImageAdapter(view.Context, imageList);

            mPager = view.FindViewById<ViewPager>(Resource.Id.pager);
            // mPager.Adapter = mAdapter;
            mPager.Adapter = adapter;
            var indicator = view.FindViewById<CirclePageIndicator>(Resource.Id.indicator);
            mIndicator = indicator;
            indicator.SetViewPager(mPager);
            indicator.SetSnap(true);

            return view;
        }
    }
}