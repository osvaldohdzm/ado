﻿using System;
using System.Collections.Generic;
using AdopticomApp.Data;
using AdopticomApp.Models;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Support.V4.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace AdopticomApp.Droid.Views
{
    public class PublicationsFragments : Fragment
    {
        private List<AnimalPublications> followsList;
        private AnimalCatalogAdapter mAdapter;
        private AnimalCatalog mAnimalCatalog;
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView mRecyclerView;

        public static PublicationsFragments NewInstance()
        {
            var frag1 = new PublicationsFragments
            {
                Arguments = new Bundle()
            };
            return frag1;
        }


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.publications_fragment, container, false);

            Button b = view.FindViewById<Button>(Resource.Id.button2);
            b.Click += delegate
            {
                HomeActivity myActivity = (HomeActivity)Activity;
                myActivity.ShowEditPublicationActivity();
            };

            List<AnimalPublications> animalPublications;
            User user = GetCurrentUser();

            if (user != null)
            {
                SQLiteDbHandler sQLite = new SQLiteDbHandler();
                animalPublications = sQLite.selectAnimalByUserIdPublications(user.Id);


                mAnimalCatalog = new AnimalCatalog(animalPublications);
                mRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
                mLayoutManager = new LinearLayoutManager(view.Context);
                mRecyclerView.SetLayoutManager(mLayoutManager);
                mAdapter = new AnimalCatalogAdapter(mAnimalCatalog, 1);
                mAdapter.ItemClick += OnItemClick;
                mRecyclerView.SetAdapter(mAdapter);
            }
            


            return view;
        }

        private void B_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        // Handler for the item click event:
        private void OnItemClick(object sender, int position)
        {
            var theAdapter = (AnimalCatalogAdapter) sender;


            // Display a toast that briefly shows the enumeration of the selected photo:
            var photoNum = position + 1;
            Toast.MakeText(View.Context, "This is photo number" + photoNum + " es " + theAdapter.numberRecyclerview,
                ToastLength.Short).Show();

            var myActivity = (HomeActivity) Activity;
            myActivity.ShowDetailsFragment(1);
        }


        private User GetCurrentUser()
        {
            ISharedPreferences getPrefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
            ISharedPreferencesEditor editor = getPrefs.Edit();
            string email = getPrefs.GetString("currenUserEmail", "");
            SQLiteDbHandler sQLite = new SQLiteDbHandler();
            List<User> mUser = sQLite.selectUserByEmail(email);

            return mUser[0];
        }
    }
}