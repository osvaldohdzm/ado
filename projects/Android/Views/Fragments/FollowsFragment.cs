﻿using System.Collections.Generic;
using AdopticomApp.Data;
using AdopticomApp.Models;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace AdopticomApp.Droid.Views
{
    public class FollowsFragment : Fragment
    {
        private List<AnimalPublications> followsList;
        private AnimalCatalogAdapter mAdapter;
        private AnimalCatalog mAnimalCatalog;
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView mRecyclerView;

        public static FollowsFragment NewInstance()
        {
            var frag1 = new FollowsFragment
            {
                Arguments = new Bundle()
            };
            return frag1;
        }


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.follows_fragment, container, false);

            SQLiteDbHandler sQLite;
            List<AnimalPublications> animalPublications;
            sQLite = new SQLiteDbHandler();
            animalPublications = sQLite.selectAnimalPublications();
            var followsList = animalPublications;

            mAnimalCatalog = new AnimalCatalog(followsList);
            mRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            mLayoutManager = new LinearLayoutManager(view.Context);
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mAdapter = new AnimalCatalogAdapter(mAnimalCatalog, 1);
            mAdapter.ItemClick += OnItemClick;
            mRecyclerView.SetAdapter(mAdapter);


            return view;
        }

        // Handler for the item click event:
        private void OnItemClick(object sender, int position)
        {
            var theAdapter = (AnimalCatalogAdapter) sender;


            // Display a toast that briefly shows the enumeration of the selected photo:
            var photoNum = position + 1;
            Toast.MakeText(View.Context, "This is photo number" + photoNum + " es " + theAdapter.numberRecyclerview,
                ToastLength.Short).Show();

            var myActivity = (HomeActivity) Activity;
            myActivity.ShowDetailsFragment(1);
        }
    }
}