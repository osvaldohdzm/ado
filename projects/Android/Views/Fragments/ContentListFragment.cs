﻿using System.Collections.Generic;
using AdopticomApp.Data;
using AdopticomApp.Models;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;

namespace AdopticomApp.Droid.Views
{
    public class ContentListFragment : Fragment
    {
        SQLiteDbHandler sQLite;
        List<AnimalPublications> animalPublications;
        List<AnimalPublications> dogsCatalog;
        List<AnimalPublications> catsCatalog;

        private readonly int selectedID;
        private ContentListAdapter adapter;
        private ListView AnimalListView;
        private List<string> content;
        private List<string> ejercicios;
        private List<string> examenes;
        private Context myContext;
        private List<string> recursos;
        private List<string> temas;
        private View view;

        public ContentListFragment(int optionID)
        {
            selectedID = optionID;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.listscreen, container, false);

            myContext = view.Context;
            var activity = (Activity) view.Context;

            AnimalListView = view.FindViewById<ListView>(Resource.Id.myListView);

            content = Getcontent();
            adapter = new ContentListAdapter(Activity, content);

            AnimalListView.Adapter = adapter;

            AnimalListView.ItemClick += AnimalListView_ItemClick;


            return view;
        }

        private void AnimalListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var selected = content[e.Position];
            Toast.MakeText(myContext, selected, ToastLength.Short).Show();
        }

        private List<string> Getcontent()
        {
            List<string> mContent = new List<string>();
            sQLite = new SQLiteDbHandler();
            for (int i = 0; i < animalPublications.Count; i++)
            {
                mContent.Add(animalPublications[i].Name);
            }

            return null;
        }
    }
}