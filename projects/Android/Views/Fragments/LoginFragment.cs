﻿using AdopticomApp.Data;
using AdopticomApp.Models;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AdopticomApp.Droid.Views
{
    public class LoginFragment : Fragment
    {
        SQLiteDbHandler sQLite;


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            SQLiteDbHandler sQLite = new SQLiteDbHandler();

            View view = inflater.Inflate(Resource.Layout.login, container, false);
            Button button1 = view.FindViewById<Button>(Resource.Id.email_sign_in_button);

            AutoCompleteTextView username = view.FindViewById<AutoCompleteTextView>(Resource.Id.email);
            TextInputEditText Password = view.FindViewById<TextInputEditText>(Resource.Id.password);

            Button button2 = view.FindViewById<Button>(Resource.Id.btnLinkToRegisterScreen);
            Button button3 = view.FindViewById<Button>(Resource.Id.btnLinkToRegisterScreen2);
            Button button = view.FindViewById<Button>(Resource.Id.login_button_facebook);
            LoginRegisterActivity myActivity = (LoginRegisterActivity)Activity;
            Snackbar snackBar = Snackbar.Make(myActivity.FindViewById(Android.Resource.Id.Content), "No hay conexión",
                Snackbar.LengthIndefinite);
            snackBar.Dismiss();
            if (myActivity.IsConnected())
            {
                button1.Click += delegate
                {
                    sQLite = new SQLiteDbHandler();
                    List<User> mUser = sQLite.selectUsers();


                    bool registered = false;


                    for (int i = 0; i < mUser.Count; i++)
                    {
                        if (username.Text == mUser[i].Email && Password.Text == mUser[i].Password)
                        {                            
                            ISharedPreferences getPrefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
                            ISharedPreferencesEditor editor = getPrefs.Edit();
                            editor.PutString("currenUserEmail", mUser[i].Email);
                            editor.Apply();

                            registered = true;
                        }
                    }

                    

                    if (registered)
                    {
                        StartActivity(new Intent(Activity, typeof(HomeActivity)));
                    }
                };
                button2.Click += delegate { myActivity.ChangeFragment(new RegisterIndividualFragment()); };
                button3.Click += delegate { myActivity.StartActivity(new Intent(Android.App.Application.Context, typeof(RegisterAssociationActivity))); };
            }
            else
            {
                snackBar.Show();
                snackBar.SetAction("OK", v => { });
                RefreshFragment();
            }

            return view;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
        }

        public override void OnResume()
        {
            base.OnResume();
        }

        private async void RefreshFragment()
        {
            LoginFragment fragmentLogin = new LoginFragment();
            LoginRegisterActivity myActivity = (LoginRegisterActivity)Activity;

            while (!myActivity.IsConnected())
            {
                await Task.Delay(1000);
            }

            myActivity.ChangeFragment(fragmentLogin);
        }
    }
}