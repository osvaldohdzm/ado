﻿using System;
using System.Collections.Generic;
using AdopticomApp.Data;
using AdopticomApp.Models;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Preferences;
using Android.Support.V7.App;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace AdopticomApp.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/MainTheme.Login")]
    public class LoginRegisterActivity : AppCompatActivity

    {
        public LoginFragment fragmentLogin = new LoginFragment();
        public RegisterIndividualFragment fragmentIndividualRegister = new RegisterIndividualFragment();
        public Context mContext;
        private FragmentManager mFragmentManager;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.login_register);

            mFragmentManager = SupportFragmentManager;

            mContext = this;

            ChangeFragment(fragmentLogin);


        }

        public void SecondFragment()
        {
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.FrameContainer, fragmentIndividualRegister)
                .AddToBackStack(null).Commit();
        }

       

        public void ChangeFragment(Fragment Fragmento)
        {
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.FrameContainer, Fragmento)
                .AddToBackStack(null).Commit();
        }


        public bool IsConnected()
        {
            var info = GetNetworkInfo(mContext);
            return info != null && info.IsConnected;
        }

        public static NetworkInfo GetNetworkInfo(Context context)
        {
            var cm = (ConnectivityManager)context.GetSystemService(ConnectivityService);
            return cm.ActiveNetworkInfo;
        }

    }
}