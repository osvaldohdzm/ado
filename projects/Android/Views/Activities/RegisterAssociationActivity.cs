﻿

using AdopticomApp.Data;
using AdopticomApp.Models;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Preferences;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Text;
using Android.Text.Method;
using Android.Widget;
using System;
using System.Collections.Generic;

namespace AdopticomApp.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/Theme.MyTheme", LaunchMode = LaunchMode.SingleTop,
        HardwareAccelerated = true)]
    public class RegisterAssociationActivity : AppCompatActivity
    {
        public LoginFragment fragmentLogin = new LoginFragment();
        public static readonly int PickImageId = 1000;
        private ImageView _imageView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.register_association);

            var Text1 = "Aceptar <a href=\"http://google.com\">terminos y condiciones.</a>";
            _imageView = FindViewById<ImageView>(Resource.Id.imageView1);
            Button button = FindViewById<Button>(Resource.Id.photo);

            AutoCompleteTextView AppCompatAutoCompleteTextViewName = FindViewById<AutoCompleteTextView>(Resource.Id.register_name);
            AutoCompleteTextView AppCompatAutoCompleteTextViewEmail = FindViewById<AutoCompleteTextView>(Resource.Id.register_email);
            TextInputEditText AppCompatAutoCompleteTextViewPassword = FindViewById<TextInputEditText>(Resource.Id.register_epassword);

            button.Click += ButtonOnClick;

            var TextView1 = FindViewById<CheckBox>(Resource.Id.checkbox);
#pragma warning disable CS0618 // 'Html.FromHtml(string)' está obsoleto: 'deprecated'
            TextView1.TextFormatted = Html.FromHtml(Text1);
#pragma warning restore CS0618 // 'Html.FromHtml(string)' está obsoleto: 'deprecated'
            TextView1.MovementMethod = LinkMovementMethod.Instance;


            var button3 = FindViewById<Button>(Resource.Id.register_email_login_in_button);
            button3.Click += delegate
            {
                RegisterNewUser(new User { Email = AppCompatAutoCompleteTextViewEmail.Text, Name = AppCompatAutoCompleteTextViewName.Text, Type = "Association", Password = AppCompatAutoCompleteTextViewPassword.Text });

                StartActivity(new Intent(Application.Context, typeof(HomeActivity)));
                Toast.MakeText(this, "Registro exitoso.", ToastLength.Long).Show();
            };



        }

        // Create a Method ButtonOnClick.   
        private void ButtonOnClick(object sender, EventArgs eventArgs)
        {
            Intent = new Intent();
            Intent.SetType("image/*");
            Intent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(Intent, "Select Picture"), PickImageId);
        }
        // Create a Method OnActivityResult(it is select the image controller)   
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if ((requestCode == PickImageId) && (resultCode == Result.Ok) && (data != null))
            {
                Android.Net.Uri uri = data.Data;
                _imageView.SetImageURI(uri);
            }
        }
        

        public void RegisterNewUser(User u)
        {
            bool registered = false;
            SQLiteDbHandler sQLite = new SQLiteDbHandler();
            List<User> mUser = sQLite.selectUsers();
            for (int i = 0; i < mUser.Count; i++)
            {
                if (mUser[i] == u)
                {
                    registered = true;
                }
            }

            if (registered == false)
            {
                sQLite.insertUser(u);
                ISharedPreferences getPrefs = PreferenceManager.GetDefaultSharedPreferences(Android.App.Application.Context);
                ISharedPreferencesEditor editor = getPrefs.Edit();
                editor.PutString("currenUserEmail", u.Email);
                editor.Apply();
            }
        }
    }
}
