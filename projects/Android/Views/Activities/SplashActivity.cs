﻿using System;
using System.Threading.Tasks;
using AdopticomApp.Data;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Support.V7.App;
using Android.Widget;

namespace AdopticomApp.Droid.Views
{
    [Activity(Label = "@string/app_name", Icon = "@mipmap/ic_launcher", Theme = "@style/MainTheme.Splash",
        MainLauncher = true, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {
        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);



          


            
                                          
        }

        // Launches the startup task
        protected override void OnResume()
        {
            base.OnResume();

            RunOnUiThread(() =>
            {

                ISharedPreferences getPrefs = PreferenceManager.GetDefaultSharedPreferences(BaseContext);
                bool isFirstStart = getPrefs.GetBoolean("firstStart", true);

                if (isFirstStart)
                {

                    SimulateStartup();
                    Finish();
                    ISharedPreferencesEditor editor = getPrefs.Edit();
                    editor.PutBoolean("firstStart", false);
                    editor.Apply();
                }
                else
                {
                    ISharedPreferencesEditor editor = getPrefs.Edit();
                    string email = getPrefs.GetString("currenUserEmail", "");

                    if (email == "")
                    {
                        StartActivity(new Intent(Application.Context, typeof(LoginRegisterActivity)));
                    }
                    else
                    {
                        StartActivity(new Intent(Application.Context, typeof(HomeActivity)));
                    }

                    SQLiteDbHandler sQLite = new SQLiteDbHandler();
                    sQLite.loadDatabase();


                }
            }


          );

        }

        // Prevent the back button from canceling the startup process
        public override void OnBackPressed()
        {
        }

        private async void SimulateStartup()
        {
            await Task.Delay(0);
            StartActivity(new Intent(Application.Context, typeof(SlideIntroActivity)));
        }
    }
}
