﻿using AdopticomApp.Data;
using AdopticomApp.Models;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Preferences;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace AdopticomApp.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/Theme.MyTheme", LaunchMode = LaunchMode.SingleTop,
        HardwareAccelerated = true)]
    public class HomeActivity : AppCompatActivity
    {
        private DrawerLayout drawerLayout;
        private FragmentManager mFragmentManager;
        private NavigationView navigationView;

        private int oldPosition = -1;
        private IMenuItem previousItem;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Current user identification
            ISharedPreferences getPrefs = PreferenceManager.GetDefaultSharedPreferences(BaseContext);
            ISharedPreferencesEditor editor = getPrefs.Edit();
            string email = getPrefs.GetString("currenUserEmail", "");
            SQLiteDbHandler sQLite = new SQLiteDbHandler();
            List<User> mUser = sQLite.selectUserByEmail(email);
            Toast.MakeText(this, "Bienvenido " + mUser[0].Name, ToastLength.Short).Show();

            SetContentView(Resource.Layout.fragment_home_user);

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if (toolbar != null)
            {
                SetSupportActionBar(toolbar);
                SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                SupportActionBar.SetHomeButtonEnabled(true);
            }
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            mFragmentManager = SupportFragmentManager;

            

            //handle navigation
            navigationView.NavigationItemSelected += (sender, e) =>
            {
                if (previousItem != null)
                    previousItem.SetChecked(false);

                navigationView.SetCheckedItem(e.MenuItem.ItemId);

                previousItem = e.MenuItem;

                if (e.MenuItem.ItemId == Resource.Id.nav_home_1)
                    ListItemClicked(1);
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_2)
                    ListItemClicked(2);
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_3)
                    ListItemClicked(3);
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_4)
                    ListItemClicked(4);
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_5)
                    ListItemClicked(5);
                else if (e.MenuItem.ItemId == Resource.Id.nav_log_out)
                    ListItemClicked(6);

                drawerLayout.CloseDrawers();
            };


            //if first time you will want to go ahead and click first item.
            if (savedInstanceState == null)
            {
                navigationView.SetCheckedItem(Resource.Id.nav_home_1);
                ListItemClicked(1);
            }
        }

        private void ListItemClicked(int position)
        {
            //this way we don't load twice, but you might want to modify this a bit.
            if (position == oldPosition)
                return;

            oldPosition = position;

            Fragment fragment = null;
            switch (position)
            {
                case 1:
                    fragment = HomeFragment.NewInstance();
                    SupportFragmentManager.BeginTransaction()
               .Replace(Resource.Id.content_frame, fragment)
               .Commit();
                    break;
                case 2:
                    fragment = Profilefragment.NewInstance();
                    SupportFragmentManager.BeginTransaction()
               .Replace(Resource.Id.content_frame, fragment)
               .Commit();
                    break;
                case 3:
                    fragment = NewsFragment.NewInstance();
                    SupportFragmentManager.BeginTransaction()
               .Replace(Resource.Id.content_frame, fragment)
               .Commit();
                    break;
                case 4:
                    fragment = PublicationsFragments.NewInstance();
                    SupportFragmentManager.BeginTransaction()
               .Replace(Resource.Id.content_frame, fragment)
               .Commit();
                    break;
                case 5:
                    fragment = FollowsFragment.NewInstance();
                    SupportFragmentManager.BeginTransaction()
               .Replace(Resource.Id.content_frame, fragment)
               .Commit();
                    break;
                case 6:
                    var intent = new Intent(this, typeof(LoginRegisterActivity));
                    StartActivity(intent);
                    break;
                default:

                    break;
            }


        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(GravityCompat.Start);
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        public void ShowListFragment()
        {
            var myFragment = new AnimalsListFragment();
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, myFragment)
                .AddToBackStack(null).Commit();
        }

        public void ShowFragmentOption(int optionID)
        {
            var myFragment = new ContentListFragment(optionID);
            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, myFragment)
                .AddToBackStack(null).Commit();
        }

        public void ShowDetailsFragment(int publicationId)
        {
            var myFragment = new DetailsFragment();
            Bundle args = new Bundle();
            args.PutInt("publicationId", publicationId);
            myFragment.Arguments = args;

            SupportFragmentManager.BeginTransaction().Replace(Resource.Id.content_frame, myFragment)
                .AddToBackStack(null).Commit();
        }

        public void ShowEditPublicationActivity()
        {
            var intent = new Intent(this, typeof(EditPublicationActivity));
            StartActivity(intent);
        }
    }
}
