﻿

using AdopticomApp.Data;
using AdopticomApp.Models;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Preferences;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Text;
using Android.Text.Method;
using Android.Widget;
using System;
using System.Collections.Generic;

namespace AdopticomApp.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/Theme.MyTheme", LaunchMode = LaunchMode.SingleTop,
        HardwareAccelerated = true)]
    public class NewPublicationActivity : AppCompatActivity
    {
        public LoginFragment fragmentLogin = new LoginFragment();
        public static readonly int PickImageId = 1000;
        private ImageView _imageView;

        protected override void OnCreate(Bundle bundle)
        {

            // Current user identification
            ISharedPreferences getPrefs = PreferenceManager.GetDefaultSharedPreferences(BaseContext);
            ISharedPreferencesEditor editor = getPrefs.Edit();
            string email = getPrefs.GetString("currenUserEmail", "");
            SQLiteDbHandler sQLite = new SQLiteDbHandler();
            List<User> mUser = sQLite.selectUserByEmail(email);


            SetContentView(Resource.Layout.new_publication_fragment);

            EditText txtv1 = FindViewById<EditText>(Resource.Id.editText1);
            EditText txtv2 = FindViewById<EditText>(Resource.Id.editText2);
            EditText txtv3 = FindViewById<EditText>(Resource.Id.editText3);
            EditText txtv4 = FindViewById<EditText>(Resource.Id.editText4);
            Button button = FindViewById<Button>(Resource.Id.button1);
            _imageView = FindViewById<ImageView>(Resource.Id.imageView1);
            RadioButton radio_dog = FindViewById<RadioButton>(Resource.Id.radio_dog);
            RadioButton radio_cat = FindViewById<RadioButton>(Resource.Id.radio_cat);



            string animal = "Dog";
            if (radio_dog.Checked)
            {
                animal = "Dog";
            } else if (radio_cat.Checked)
            {
                animal = "Cat";
            }

            button.Click += ButtonOnClick;



            var button3 = FindViewById<Button>(Resource.Id.button2);
            button3.Click += delegate
            {
                RegisterNewPublication(new AnimalPublications {Animal = animal, Name= txtv1.Text, Breeds = txtv2.Text, Health = txtv3.Text, Description = txtv4.Text,UserId = mUser[0].Id, AnimalPrincipalPicture = Resource.Drawable.default_animal });
                Finish();
            };


        }

        // Create a Method ButtonOnClick.   
        private void ButtonOnClick(object sender, EventArgs eventArgs)
        {
            Intent = new Intent();
            Intent.SetType("image/*");
            Intent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(Intent, "Select Picture"), PickImageId);
        }
        // Create a Method OnActivityResult(it is select the image controller)   
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            if ((requestCode == PickImageId) && (resultCode == Result.Ok) && (data != null))
            {
                Android.Net.Uri uri = data.Data;
                _imageView.SetImageURI(uri);
            }
        }
        

        public void RegisterNewPublication(AnimalPublications u)
        {
            SQLiteDbHandler sQLite = new SQLiteDbHandler();
            sQLite.insertAnimalPublication(u);
        }
    }
}
