package md5083c4405ef79812617e1201643b01381;


public class AnimalViewHolder
	extends android.support.v7.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("AdopticomApp.Droid.Views.AnimalViewHolder, AdopticomApp", AnimalViewHolder.class, __md_methods);
	}


	public AnimalViewHolder (android.view.View p0)
	{
		super (p0);
		if (getClass () == AnimalViewHolder.class)
			mono.android.TypeManager.Activate ("AdopticomApp.Droid.Views.AnimalViewHolder, AdopticomApp", "Android.Views.View, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
