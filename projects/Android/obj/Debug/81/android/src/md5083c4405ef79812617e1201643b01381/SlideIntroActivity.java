package md5083c4405ef79812617e1201643b01381;


public class SlideIntroActivity
	extends md592f5fcbd77a673eb45469636c117f080.AppIntro2
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("AdopticomApp.Droid.Views.SlideIntroActivity, AdopticomApp", SlideIntroActivity.class, __md_methods);
	}


	public SlideIntroActivity ()
	{
		super ();
		if (getClass () == SlideIntroActivity.class)
			mono.android.TypeManager.Activate ("AdopticomApp.Droid.Views.SlideIntroActivity, AdopticomApp", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
