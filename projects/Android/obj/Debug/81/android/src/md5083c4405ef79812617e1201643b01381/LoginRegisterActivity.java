package md5083c4405ef79812617e1201643b01381;


public class LoginRegisterActivity
	extends android.support.v7.app.AppCompatActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("AdopticomApp.Droid.Views.LoginRegisterActivity, AdopticomApp", LoginRegisterActivity.class, __md_methods);
	}


	public LoginRegisterActivity ()
	{
		super ();
		if (getClass () == LoginRegisterActivity.class)
			mono.android.TypeManager.Activate ("AdopticomApp.Droid.Views.LoginRegisterActivity, AdopticomApp", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
