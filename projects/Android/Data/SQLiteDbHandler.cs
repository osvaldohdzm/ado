﻿using AdopticomApp.Models;
using Android.Util;
using SQLite;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;


namespace AdopticomApp.Data
{
    public class SQLiteDbHandler
    {
        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public bool createDatabase()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.CreateTable<User>();
                    connection.CreateTable<AnimalPublications>();
                    connection.CreateTable<Picture>();
                    connection.CreateTable<PublicationReports>();
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool dropDatabase()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.DropTable<User>();
                    connection.DropTable<AnimalPublications>();
                    connection.DropTable<Picture>();
                    connection.DropTable<PublicationReports>();
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public List<AnimalPublications> selectAnimalByUserIdPublications(int id)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {

                    return connection.Query<AnimalPublications>("SELECT * FROM AnimalPublications Where UserId=?", id);
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool loadDatabase()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    dropDatabase();
                    createDatabase();
                    User user1 = new User { Email = "osvaldo.hdz.m@outlook.com", Name = "Osvaldo", Type = "Individual", Password = "osvaldo" };
                    insertUser(user1);
                    insertUser(new User { Email = "admin", Name = "admin", Type = "Administrator", Password = "admin" });
                    insertUser(new User { Email = "contacto@patitas.com", Name = "Mundo Patitas", Type = "Association", Password = "patitas" });
                    insertAnimalPublication(new AnimalPublications { AnimalPrincipalPicture = Resource.Drawable.gato01, Name = "Cloy", Breeds = "Azul ruso", Animal = "Cat", UserId = user1.Id });
                    insertAnimalPublication(new AnimalPublications { AnimalPrincipalPicture = Resource.Drawable.gato02, Name = "Lilly", Breeds = "Scottish Fold", Animal = "Cat" });
                    insertAnimalPublication(new AnimalPublications { AnimalPrincipalPicture = Resource.Drawable.gato03, Name = "Micado", Breeds = "Siamés", Animal = "Cat" });
                    insertAnimalPublication(new AnimalPublications { AnimalPrincipalPicture = Resource.Drawable.perro01, Name = "Spanky", Breeds = "Bulldog", Animal = "Dog", UserId = user1.Id });
                    insertAnimalPublication(new AnimalPublications { AnimalPrincipalPicture = Resource.Drawable.perro02, Name = "Peter", Breeds = "Pastor alemán", Animal = "Dog" });
                    insertAnimalPublication(new AnimalPublications { AnimalPrincipalPicture = Resource.Drawable.perro04, Name = "Naty", Breeds = "Beagle", Animal = "Dog" });
                    insertAnimalPublication(new AnimalPublications { AnimalPrincipalPicture = Resource.Drawable.perro05, Name = "Randy", Breeds = "Golden retriever", Animal = "Dog" });

                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public List<User> selectUserByEmail(string email)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {

                    return connection.Query<User>("SELECT * FROM User Where Email=?", email);
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool insertUser(User User)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Insert(User);

                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }


        }
        public List<User> selectUsers()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    return connection.Table<User>().ToList();
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool updateUser(User User)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Query<User>("UPDATE User set name=?, type=?, email=? Where Id=?", User.Name, User.Type, User.Email);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        //Delete Data Operation  

        public bool removeUser(User User)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Delete(User);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        //Select Operation  

        public List<User> selectUser(int Id)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    
                    return connection.Query<User>("SELECT * FROM User Where Id=?", Id);
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }
    

        public bool insertAnimalPublication(AnimalPublications AnimalPublication)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Insert(AnimalPublication);

                    // Debug database
                    /* 
                    Console.WriteLine("Reading data");
                    var table = connection.Table<AnimalPublication>();
                    foreach (var s in table)
                    {
                        Console.WriteLine(s.Name + " " + s.Password);
                    }
                    */

                                       
                   



                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }


        }
        public List<AnimalPublications> selectAnimalPublications()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    return connection.Table<AnimalPublications>().ToList();
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool updateAnimalPublication(AnimalPublications AnimalPublication)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Query<AnimalPublications>("UPDATE AnimalPublication set name=?, type=?, email=? Where Id=?", AnimalPublication.Name, AnimalPublication.AnimalPrincipalPicture);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        //Delete Data Operation  

        public bool removeAnimalPublication(AnimalPublications AnimalPublication)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Delete(AnimalPublication);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        //Select Operation  

        public List<AnimalPublications> selectAnimalPublication(int Id)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {

                    return connection.Query<AnimalPublications>("SELECT * FROM AnimalPublications Where Id=?", Id);
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool insertPicture(Picture Picture)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Insert(Picture);

                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }


        }
        public List<Picture> selectPictures()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    return connection.Table<Picture>().ToList();
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool updatePicture(Picture Picture)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Query<Picture>("UPDATE Picture set name=?, type=?, email=? Where Id=?", Picture.PictureId, Picture.PublicationId);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        //Delete Data Operation  

        public bool removePicture(Picture Picture)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Delete(Picture);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        //Select Operation  

        public bool selectPicture(int Id)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Query<Picture>("SELECT * FROM Picture Where Id=?", Id);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

        public bool insertPublicationReports(PublicationReports PublicationReports)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Insert(PublicationReports);

                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }


        }
        public List<PublicationReports> selectPublicationReportss()
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    return connection.Table<PublicationReports>().ToList();
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return null;
            }
        }

        public bool updatePublicationReports(PublicationReports PublicationReports)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Query<PublicationReports>("UPDATE PublicationReports set name=?, type=?, email=? Where Id=?", PublicationReports.PublicationId);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        //Delete Data Operation  

        public bool removePublicationReports(PublicationReports PublicationReports)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Delete(PublicationReports);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }
        //Select Operation  

        public bool selectPublicationReports(int Id)
        {
            try
            {
                using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Adopticom.db")))
                {
                    connection.Query<PublicationReports>("SELECT * FROM PublicationReports Where Id=?", Id);
                    return true;
                }
            }
            catch (SQLiteException ex)
            {
                Log.Info("SQLiteEx", ex.Message);
                return false;
            }
        }

    }
}