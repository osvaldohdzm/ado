using SQLite;
using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;

namespace AdopticomApp.Models
{
    public class User
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
        public int FollowedPublicationsListId { get; set; }
        public int ReportedPublicationsListId { get; set; }
    }
}