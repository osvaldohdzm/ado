﻿using SQLite;
using SQLiteNetExtensions.Attributes;

namespace AdopticomApp.Models
{
    public class AnimalPublications

    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Breeds { get; set; }
        public string Health { get; set; }
        public string Description { get; set; }
        public string Animal { get; set; }
        public int AnimalPrincipalPicture { get; set; }
    }
}