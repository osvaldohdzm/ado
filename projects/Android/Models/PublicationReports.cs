﻿using SQLite;
using SQLiteNetExtensions.Attributes;

namespace AdopticomApp.Models
{
    public class PublicationReports

    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string PublicationId { get; set; }
        public string Description { get; set; }
    }
}