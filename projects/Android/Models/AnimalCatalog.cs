﻿using System;
using System.Collections.Generic;

namespace AdopticomApp.Models
{
    // Animal catalog: holds image resource IDs and caption:
    public class AnimalCatalog
    {
        private readonly List<AnimalPublications> mAnimals;

        // Random number generator for shuffling the Animals:
        private readonly Random mRandom;

        // Create an instance copy of the built-in Animal list and
        // create the random number generator:
        public AnimalCatalog(List<AnimalPublications> catalog)
        {
            mAnimals = catalog;
            mRandom = new Random();
        }

        // Return the number of Animals in the Animal catalog:
        public int NumAnimals => mAnimals.Count;

        // Indexer (read only) for accessing a Animal:
        public AnimalPublications this[int i] => mAnimals[i];

        // Pick a random Animal and swap it with the top:
        public int RandomSwap()
        {
            // Save the Animal at the top:
            var tmpAnimal = mAnimals[0];

            // Generate a next random index between 1 and 
            // Length (noninclusive):
            var rnd = mRandom.Next(1, mAnimals.Count);

            // Exchange top Animal with randomly-chosen Animal:
            mAnimals[0] = mAnimals[rnd];
            mAnimals[rnd] = tmpAnimal;

            // Return the index of which Animal was swapped with the top:
            return rnd;
        }

        // Shuffle the order of the Animals:
        public void Shuffle()
        {
            // Use the Fisher-Yates shuffle algorithm:
            for (var idx = 0; idx < mAnimals.Count; ++idx)
            {
                // Save the Animal at idx:
                var tmpAnimal = mAnimals[idx];

                // Generate a next random index between idx (inclusive) and 
                // Length (noninclusive):
                var rnd = mRandom.Next(idx, mAnimals.Count);

                // Exchange Animal at idx with randomly-chosen (later) Animal:
                mAnimals[idx] = mAnimals[rnd];
                mAnimals[rnd] = tmpAnimal;
            }
        }
    }
}