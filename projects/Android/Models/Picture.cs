﻿using SQLite;
using SQLiteNetExtensions.Attributes;

namespace AdopticomApp.Models
{
    public class Picture

    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string PublicationId { get; set; }
        public string PictureId { get; set; }
    }
}